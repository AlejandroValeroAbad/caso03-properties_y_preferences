package aplicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class AppProperties {

	public static void main(String[] args) {
		System.out.println("---------------- PROPERTIES ----------------");
		try {
			usandoProperties();
		} catch (IOException ex) {
			System.err.println(ex);
		}

		
	}

	private static void usandoProperties() throws FileNotFoundException, IOException {

		// Acceso individual a una propiedad del sistema
		System.out.println("Propiedad de sistema: carpeta de usuario -> " + System.getProperty("user.home"));
		// Otras properties:
		// https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html
		System.out.println("-----------------------------------");

		// Acceso a todas las propiedades del sistema
		Properties ps = System.getProperties();
		// Mostrar todas las propiedades
		ps.list(System.out);
		System.out.println("-----------------------------------");

		// Podriamos guardarlas en un fichero nuestro
		File f = new File(System.getProperty("user.home"), "copia.properties");
		ps.store(new FileOutputStream(f), "Copia propiedades de sistema");

		// Creación de un fichero propio de propiedades
		File f1 = new File(System.getProperty("user.home"), "mispropiedades.properties");
		Properties p1 = new Properties();
		p1.setProperty("usuario", "Sergio");
		p1.setProperty("opcion1", "A");
		p1.setProperty("opcion2", "C");
		p1.setProperty("color", "Rojo");
		p1.list(System.out);
		p1.store(new FileOutputStream(f1), "Mis propiedades");
		System.out.println("-----------------------------------");
		

		// Lectura de nuestro fichero de propiedades
		Properties p2 = new Properties();
		// Cargamos las propiedades desde el fichero a memoria
		p2.load(new FileInputStream(f1));
		p2.list(System.out);
		System.out.println("-----------------------------------");
		System.out.println("El valor de la clave 'opcion1' es: " + p2.getProperty("opcion1"));
		System.out.println("El valor de la propiedad 'color' es: " + p2.getProperty("color"));

		// Recorrer todas las claves de una en una:
		Enumeration<Object> claves = p2.keys();
		while (claves.hasMoreElements()) {
			String clave = (String) claves.nextElement();
			String valor = p2.getProperty(clave);
			System.out.println(clave + " = " + valor );
		}

		// Modificación: si la propiedad existe la modifica sino la crea.
		p2.setProperty("usuario", "Sergi");
		p2.setProperty("color", "Azul");
		p2.setProperty("opcion3", "C");
		p2.setProperty("opcion4", "D");

		// Borrado
		p2.remove("opcion4");
		
		// Finalmente, debemos guardar.
		p2.store(new FileOutputStream(f1), "Mis propiedades");
	}

	

}
