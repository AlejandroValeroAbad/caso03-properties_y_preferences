package aplicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class AppPreferences {

	public static void main(String[] args) {

		System.out.println("---------------- PREFERENCES ----------------");
		try {
			usandoPreferences();
		} catch (BackingStoreException | IOException ex) {
			System.err.println(ex);
		}

	}

	private static void usandoPreferences() throws BackingStoreException, FileNotFoundException, IOException {
		// Los archivos de preferencias pueden estar ubicados en la ruta de sistema o de
		// usuario
		// Ruta de sistema: requiere permisos.
//        Preferences pf = Preferences.systemRoot();
//        Preferences pf = Preferences.systemNodeForPackage(Principal.class);

		// Ruta de usuario: diferentes formas de crear archivos de preferencias.
//        Preferences pf = Preferences.userRoot();
		Preferences pf = Preferences.userNodeForPackage(AppPreferences.class);
//		Preferences pf = Preferences.userRoot().node("miprograma");

		pf.put("usuario", "Sergio");
		pf.put("opcion1", "A");
		pf.putBoolean("opcion2", true);
		pf.putInt("opcion3", 50);
		pf.put("color", "Rojo");

		// Si una preferencia no existe
		String valorOpcionX = pf.get("opcionX", null);
		if (valorOpcionX != null)
			System.out.println("Existe. Su valor es: " + valorOpcionX);
		else
			System.out.println("No existe");
		// Podemos darle valor por defecto a una preferencia en caso de que no exista:
		System.out.println("Valor de OpcionX es: " + pf.get("opcionX", "Ojo! Sin valor definido"));

		String[] keys = pf.keys();
		for (String k : keys) {
			System.out.print(k + " --> ");
			System.out.println(pf.get(k, null));
		}

		// Podemos borrar una clave
		pf.remove("opcion1");
		// O modificarla: si no existe la clave se crea una entrada nueva.
		pf.putBoolean("opcion2", false);

		// O podemos borrar todo el nodo completo
		pf.removeNode();

	}

}
